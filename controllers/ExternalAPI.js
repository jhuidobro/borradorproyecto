const requestJson = require('request-json');

function getTiempoRivas (req,res) {
     console.log("GET /apitechu/getTiempoRivas");

     var eltiempoURL = "https://www.el-tiempo.net/api/json/v1/";

    //  var headers = {
    //      'Content-Type':'application/json'
    //  }

     var httpClient = requestJson.createClient(eltiempoURL);
     console.log("Client created");

     httpClient.get("provincias/28/municipios/28123/weather",
        function(err, resMLab, body) {
            console.log("Entro en la funcion");
            console.log("maxima: " + body.prediccion.dia[0].temperatura.maxima);
            console.log("minima: " + body.prediccion.dia[0].temperatura.minima);
            console.log("fecha: " + body.prediccion.dia[0].fecha);


             var response = {
               "max" : body.prediccion.dia[0].temperatura.maxima,
               "min" : body.prediccion.dia[0].temperatura.minima,

             }
             res.send(response);
          }
     )
}

module.exports.getTiempoRivas = getTiempoRivas;
