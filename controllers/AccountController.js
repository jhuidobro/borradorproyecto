const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjhp/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;





function getIbex35Data (req, res){
   console.log("GET /apitechu/getibex35data");
/*
   var fixingURL = "http://data.fixer.io/api/latest?";
   var KEY = "access_key=10e696d6bc3251f45dd9647d64de3c39";

   var headers = {
       'Content-Type':'application/json'
   }
   var path = "";

   var httpClient = requestJson.createClient(fixingURL);

   console.log("Client created");

   httpClient.get("?" + KEY,
     function(err, resMLab, body) {
       console.log("Entro en la funcion");
       console.log("BODY: " + body);
       console.log("BODY s: " + body.success);
       console.log("BODY rates: " + body.rates);
       console.log("BODY rates USD: " + body.rates.USD);
       var fxUSD = body.rates.USD;
       console.log("fxUSD: " + fxUSD);
       var response = {
         "USD" : fxUSD
       }
       res.send(response);
      //  res.send("FIN");

     }
   )

*/
}


function getAccountsV1 (req, res){
   console.log("GET /apitechu/v1/accounts");

   console.log(req.params.id);

   var query = 'q={"userId": ' + req.params.id + '}';
   console.log("query es " + query);
   console.log("la consulta es " + "accounts?" + query + "&" + mlabAPIKey);

   var httpClient = requestJson.createClient(mlabBaseURL);

   console.log("Client created");

   httpClient.get("accounts?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       console.log("Entro en la funcion");

       if (err) {
          console.log("Error recuperando las cuentas del usuario " + req.params.id);
          console.log("Error: " + err);
          var response = {
            "mensaje" : "Error recuperando las cuentas del usuario " + req.params.id
          }
          res.status(500);
          res.send(response);
       } else {
            if (body.length == 0) {
              console.log("El usuario " +  + req.params.id + " no tiene cuentas");
              var response = {
                "mensaje" : "El usuario " +  + req.params.id + " no tiene cuentas"
              }
              res.send(response);
           } else {
              console.log(body);
              res.send(body);
           }
       }
     }
   )

 }


 function newAccountV1 (req, res){
    console.log("POST /apitechu/v1/newAccount");

    console.log("Recuperamos el numero de cuentas que tiene el usuario: " + req.params.id);
    var query = 'q={"userId": ' + req.params.id + '}';
    var httpClient = requestJson.createClient(mlabBaseURL);
    var numCta = 0;
    console.log("Client created " + mlabBaseURL);

    httpClient.get("accounts?" + query + "&c=true&" + mlabAPIKey,
      function(err, resMLab, body) {
        console.log("Entro en la funcion");

        if (err) {
           console.log("Error recuperando el nº de cuentas del usuario " +  userId);
           var response = {
             "mensaje" : "Error recuperando el nº de cuentas del usuario " + userId
           }
           res.status(500);
           res.send(response);
        } else {
             numCta = body +1;
             console.log("El siguiente nº de cuenta es: " + numCta);

             var iban = "0182 2025 020 " + req.params.id + " " + numCta;
             console.log("iban: " + iban);

             var newAccount = {
               "IBAN" : iban,
               "userId" : Number(req.params.id),
               "balance" : 0
             };
             console.log("Nueva cta: " + newAccount);

             httpClient.post("accounts?" + mlabAPIKey, newAccount,
               function(err, resMLab, body) {
                 console.log("Entro en la funcion para dar de alta la cuenta");
                 console.log("Error: " + err);
                 console.log("resMLab: " + resMLab);
                 console.log("body: " + body);

                 res.status(201);
                 var response = {
                   "mensaje" : "Cuenta añadida con exito ",
                   "IBAN" : iban
                 }

                 res.send(response);
               }
             )
        }
      }
    )
}

function newMovementV1 (req, res){
   console.log("POST /apitechu/v1/newMovement");

   var d = new Date();
   var importe = 0;

   console.log("Numero de cuenta: " + req.params.idAccount);
   console.log("Importe: " + req.body.amount);
   console.log("Divisa: " + req.body.currency);
   console.log("Abono/Cargo: " + req.body.paymenttype);
   console.log("Fecha: " + d.toJSON());

   var newMovement = {
     "IBAN" : req.params.idAccount,
     "amount" : req.body.amount,
     "currency" : req.body.currency,
     "paymenttype" : req.body.paymenttype,
     "timestamp" : d.toJSON()
   };
   console.log("Movimiento: "+ newMovement);

   var httpClient = requestJson.createClient(mlabBaseURL);
   console.log("Client created");

   httpClient.post("movements?" + mlabAPIKey, newMovement,
     function(err, resMLab, body) {
       console.log("Entro en la funcion para dar de alta el movimiento");

       // Actualizo el balance de la cuenta.
       // Para ello primero busco la cta y obtengo el balance de la misma
       var query = 'q={"IBAN": "' + req.params.idAccount + '"}';
       console.log("query es " + query);

       httpClient.get("accounts?" + query + "&" + mlabAPIKey,
          function(err, resMLab, body) {
               console.log("Entro en la funcion para recuperar el balance de la cta.");
               if (body.length == 0) {
                 console.log("Cuenta no encontrada");
                 var response = {
                   "mensaje" : "Cuenta no encontrada, no se ha podido actualizar el balance, pongase en contacto con el banco para actualizarlo, ya que le movimiento se ha registrado correctamente."
                 }

                 res.status(500);
                 res.send(response);

               } else {
                    console.log("respuesta: " + body);
                    console.log("Balance Antes de actualizar la cta: " + body[0].balance);

                    var oldBalance = body[0].balance;
                    var newBalance = 1;

                    if (req.body.paymenttype=="Cargo") {
                      newBalance = -1 * req.body.amount
                    } else {
                      newBalance = 1 * req.body.amount
                    }

                    if (req.body.currency == "EUR") {
                        console.log("La divisa es EUR");
                        newBalance = oldBalance + newBalance;
                        actualizarBalance (query, newBalance, res);

                    } else {
                        console.log("La divisa es USD, tengo que recuperar el fx...");

                        var fixingURL = "http://data.fixer.io/api/latest?";
                        var KEY = "access_key=10e696d6bc3251f45dd9647d64de3c39";

                        var httpClient = requestJson.createClient(fixingURL);

                        httpClient.get("?" + KEY,
                          function(err, resFX, body) {
                            console.log("Entro en la funcion");
                            console.log("BODY rates USD: " + body.rates.USD);
                            var fxUSD = body.rates.USD;
                            console.log("fxUSD: " + fxUSD);


                            newBalance = oldBalance + newBalance * fxUSD;
                            actualizarBalance (query, newBalance, res);

                          }
                        )
                    }


               }
     }
   )

}
)
}


function actualizarBalance (query, newBalance, resAB) {
  console.log("Entro en la funcion actualizarBalance. Nuevo balance: " + newBalance);
  console.log("Query: " + query);


  // actualizo el Balance
  var putBody = '{"$set":{"balance": ' + newBalance + '}}';
  console.log("putBody: " + putBody);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.put("accounts?" + query + "&" + mlabAPIKey, JSON.parse(putBody),

    function(errPUT, resMLabPUT, bodyPUT) {
        console.log("Balance actualizado");

        var response = {
            "msg" : "Movimiento dado de alta con éxito"
      }

      resAB.status(201);
      resAB.send(response);
    }

  )
}

function getMovementsV1 (req, res){
   console.log("GET /apitechu/v1/getMovements");
   console.log("Usuario: " + req.params.id);
   console.log("Numero de cuenta: " + req.params.idAccount);

   var query = 'q={"IBAN": "' + req.params.idAccount + '"}';
   console.log("query es " + query);
   console.log("la consulta es " + "movements?" + query + "&" + mlabAPIKey);

   var httpClient = requestJson.createClient(mlabBaseURL);

   console.log("Client created");

   httpClient.get("movements?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       console.log("Entro en la funcion");

       if (err) {
          console.log("Error recuperando los movimientos de la cuenta " + req.params.idAccount);
          console.log("Error: " + err);
          var response = {
            "mensaje" : "Error recuperando los movimientos de la cuenta " + req.params.idAccount
          }
          res.status(500);
          res.send(response);
       } else {
            if (body.length == 0) {
              console.log("La cuenta " +  req.params.idAccount + " no tiene movimientos");
              var response = {
                "mensaje" : "La cuenta " +  req.params.idAccount + " no tiene movimientos"
              }
              res.send(response);
           } else {
              console.log(body);
              res.send(body);
           }
       }
     }
   )

 }

module.exports.getAccountsV1 = getAccountsV1;
module.exports.newAccountV1 = newAccountV1;

module.exports.newMovementV1 = newMovementV1;
module.exports.getMovementsV1 = getMovementsV1;
