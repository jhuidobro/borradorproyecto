const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjhp/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function loginV1  (req, res){

console.log("POST /apitechu/v1/login");
console.log(req.body.email);
console.log(req.body.pwd);

var users = require("../usuariosmail.json");
var existe = false;
var pwdCorrecta = false;
var result = {};
var index = 0;

for (var i = 0; i < users.length ; i++) {
   // console.log(users[i].id);
   if (users[i].email == req.body.email){
     existe = true;
     index = i;
     console.log("Existe el usuario: " + req.body.email);
     // Validamos el pwd
     pwdCorrecta = users[i].pwd == req.body.pwd ?
       true : false;
     break;
   }
}

console.log("Contraseña correcta: " + pwdCorrecta);

if (pwdCorrecta) {
   users[index].logged = true;
   result.mensaje =  "Login correcto";
   result.IdUsuario =  users[index].id;
   io.writeUserDataToFile(users);
} else {
   result.mensaje = "Login Incorrecto";
}

res.send(result);
}






function loginV2(req, res) {

  console.log("POST /apitechu/v2/login");

  console.log(req.body.email);
  console.log(req.body);

  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("query es " + query);

  httpClient = requestJson.createClient(mlabBaseURL);

  httpClient.get("users?" + query + "&" + mlabAPIKey,

     function(err, resMLab, body) {

          console.log(body.length);
          if (body.length == 0) {
            console.log("Usuario no encontrado");
            var response = {
              "mensaje" : "Usuario no encontrado"
            }

            res.status(500);
            res.send(response);

          } else {
              console.log("Password de parametro " + req.body.pwd);
              console.log("Password de BBDD" + body[0].pwd);
              var isPasswordcorrect = crypt.checkpwd(req.body.pwd, body[0].pwd);
              // var isPasswordcorrect = true;
              console.log("Password correct is " + isPasswordcorrect + " - PWD: " + body[0].pwd);

       if (!isPasswordcorrect) {

         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }

         res.status(401);
         res.send(response);

       } else {

         console.log("Got a user with that email and password, logging in");

         query = 'q={"id" : ' + body[0].id +'}';

         console.log("Query for put is " + query);

         var putBody = '{"$set":{"logged":true}}';

         httpClient.put("users?" + query + "&" + mlabAPIKey, JSON.parse(putBody),

           function(errPUT, resMLabPUT, bodyPUT) {

             console.log("PUT done");

             var response = {

               "msg" : "Usuario logado con éxito",

               "idUsuario" : body[0].id

             }

             res.send(response);

           }

         )

       }
     }
     }

   );

  }


function logoutV2(req, res) {

 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(mlabBaseURL);
 console.log("Client created");

 httpClient.get("users?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     console.log("Entro en la función");
     console.log("ERR: " + err);
     console.log("users/" + query + "&" + mlabAPIKey);
     console.log(body.length);
     if (body.length == 0) {

       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }

       res.send(response);

     } else {

       console.log("Got a user with that id, logging out");

       query = 'q={"id" : ' + body[0].id +'}';

       console.log("Query for put is " + query);

       var putBody = '{"$unset":{"logged":""}}'

       httpClient.put("users?" + query + "&" + mlabAPIKey, JSON.parse(putBody),

         function(errPUT, resMLabPUT, bodyPUT) {

           console.log("PUT done");

           var response = {

             "msg" : "Usuario deslogado",

             "idUsuario" : body[0].id

           }

           res.send(response);

         }

       )
     }
   }

 );

}



function logoutV1(req, res){
  console.log("POST /apitechu/v1/logout");
  console.log(req.body.id);

  var users = require("../usuariosmail.json");
  var existe = false;
  var logged = false;
  var result = {};
  var index = 0;

  for (var i = 0; i < users.length ; i++) {
     // console.log(users[i].id);
     if (users[i].id == req.body.id){
       existe = true;
       index = i;
       console.log("Existe el usuario: " + req.body.id);
       // Validamos si está logado
       logged = users[i].logged ?
         true : false;
     }
  }

  console.log("Usuario logado: " + logged);

  if (logged) {
     delete users[index].logged;
     result.mensaje = "Logout correcto";
     result.IdUsuario = users[index].id;
     io.writeUserDataToFile(users);
  } else {
     result.mensaje = "Logout incorrecto";
  }

  res.send(result);

}



module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
