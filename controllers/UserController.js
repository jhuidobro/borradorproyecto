const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjhp/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1 (req, res){
   console.log("GET /apitechu/v1/users");

   console.log(req.query.$top);
   console.log(req.query.$count);

   var top = req.query.$top;
   var result = {};
   var users = require("../usuariosmail.json");

   if (req.query.$count == "true") {
     console.log(req.query.$count);
     result.count = users.length;
   }

   result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

    console.log("Resultado: " + result);
   res.send(result);

 }

function createUserV1(req, res){
    console.log("POST /apitechu/v1/users");

    // console.log(req.headers);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    var newUser = {
      "id" : req.headers.first_name + req.headers.first_name,
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "email" : req.headers.email,
      "pwd" : "New PWD",
    };

    var users = require('../usuariosmail.json');
    users.push(newUser);

    io.writeUserDataToFile(users);

    res.send("Usuario añadido con exito");
  }


function deleteUserv1(req, res){
     console.log("DELETE /apitechu/v1/users/:id");

     console.log("Parametros de id a eliminar: " + req.params.id);
     var users = require("../usuariosmail.json");

     var i = 0;
     var existe = false;

      // Buscamos el Id en el array con un for
       for (i = 0; i < users.length ; i++) {
          // console.log(users[i].id);
          if (users[i].id == req.params.id){
            existe = true;
            console.log("Elemento a borrar: " + i);
            console.log("Usuario a borrar: " + users[i].id);
            users.splice(i,1);
            io.writeUserDataToFile(users);
            break;
          }
       }

       console.log("Existe?: " + existe);

       var mensaje = existe ?
          "Usuario borrado" : "Usuario no encontrado";

      res.send("mensaje");
   }

function getUsersV2 (req, res){
      console.log("GET /apitechu/v2/users");


      var httpClient = requestJson.createClient(mlabBaseURL);

      console.log("Client created");

      httpClient.get("users?" + mlabAPIKey,
        function(err, resMLab, body) {
          console.log("Entro en la funcion");
          var response = !err ?
            body : {"msg" : "Error obteniendo usuarios"};
            console.log("Error " + err);
            console.log("Response " + response);
            res.send(response);

        }
      )
    }


function getUsersByIdV2 (req, res){
       console.log("GET /apitechu/v2/users/:id");
       console.log("Parametros id del usuario: " + req.params.id);

       var id = req.params.id;
       var query = 'q={"id" : ' + id + '}';
       query = 'users?' + query + '&' + mlabAPIKey;

       console.log("Query: " + query);

       var httpClient = requestJson.createClient(mlabBaseURL);

       console.log("Client created " + mlabBaseURL);

       httpClient.get(query,
         function(err, resMLab, body) {

           console.log("Entro en la funcion");
           var response = {};

           if (err) {
              response = {
               "msg" : "Error obteniendo usuario"
             };
             res.status(500);
           } else {
                if (body.length > 0) {
                  response = body[0];
                } else {
                  response = {
                    "msg" : "Usuario no encontrado"
                  };
                  res.status(404);
                }
           }

           res.send(response);

         }
       )

     }


function getUsersByIdV3 (req, res){
  console.log("GET /apitechu/v3/users/:id");
  console.log("Parametros id del usuario: " + req.params.id);

  var id = req.params.id;
  var query = 'q={"_id": {"$oid": "' + id + '"}}';
  query = 'users?' + query + '&' + mlabAPIKey;
  console.log("Query: " + query);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created " + mlabBaseURL);

  httpClient.get(query,
    function(err, resMLab, body) {
      console.log("Entro en la funcion");
      var response = {};

      if (err) {
          response = {
            "msg" : "Error obteniendo usuario"
          };
          res.status(500);
      } else {
          if (body.length > 0) {
              response = body[0];
          } else {
              response = {
                "msg" : "Usuario no encontrado"
              };
          res.status(404);
          }
      }

      res.send(response);

    }
  )
}


function createUserV2(req, res) {
    console.log("POST /apitechu/v2/users");

    console.log(req.body);
    // console.log(req.body.first_name);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.pwd);

    var newUser = {
      "id" : req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "pwd" : crypt.hash(req.body.pwd)
    };


    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client created " + mlabBaseURL);

    httpClient.post("users?" + mlabAPIKey, newUser,
      function(err, resMLab, body) {
        console.log("Entro en la funcion");
        res.status(201);
        res.send("Usuario añadido con exito");
      }
    )

}

function createUserV3(req, res) {
    console.log("POST /apitechu/v3/users");

    console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.pwd);
    console.log(req.body.phone);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "phone" : req.body.phone,
      "pwd" : crypt.hash(req.body.pwd)
    };


    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client created " + mlabBaseURL);

    httpClient.post("users?" + mlabAPIKey, newUser,
      function(err, resMLab, body) {
        console.log("Entro en la funcion");

        res.status(201);
        res.send("Usuario añadido con exito");
      }
    )

}

function createUserV4(req, res) {
    console.log("POST /apitechu/v4/users");

    var idUser = 1;

    console.log("Recuperamos el max Id.");
    var httpClient = requestJson.createClient(mlabBaseURL);

    console.log("Client created");

    httpClient.get("users?s={'id':-1}&l=1&" + mlabAPIKey,
      function(err, resMLab, body) {
        console.log("Entro en la funcion para obtener el max idUser");
        if (err) {
           console.log("Error recuperando max idUser.");
           var response = {
             "mensaje" : "Error recuperando max idUser"
           }
           res.status(500);
           res.send(response);
        } else {
             idUser = body[0].id +1;
             console.log("Usuario id: " + idUser);
             console.log("req.body.first_name: " + req.body.first_name);
             console.log("req.body.last_name: " + req.body.last_name);
             console.log("req.body.email: " + req.body.email);
             console.log("req.body.phone: " + req.body.phone);
             console.log("req.body.pwd: " + req.body.pwd);

             var newUser = {
               "id" : idUser,
               "first_name" : req.body.first_name,
               "last_name" : req.body.last_name,
               "email" : req.body.email,
               "phone" : req.body.phone,
               "pwd" : crypt.hash(req.body.pwd)
             };
             console.log("Usuario: "+ newUser);

             httpClient.post("users?" + mlabAPIKey, newUser,
               function(err, resMLab, body) {
                 console.log("Entro en la funcion para dar de alta al usuario");

                 res.status(201);
                 res.send("Usuario añadido con exito");
                 // return res.redirect('/producto/' + id)
               }
             )
      }
      }
    )
}

module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserv1 = deleteUserv1;

module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.getUsersByIdV3 = getUsersByIdV3;
module.exports.createUserV2 = createUserV2;
module.exports.createUserV3 = createUserV3;
module.exports.createUserV4 = createUserV4;
