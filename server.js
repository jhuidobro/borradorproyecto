require('dotenv').config();

const express = require('express');
const app = express();
app.use(express.json()); //esto es para que el body lo trate como JSON

var enableCORS = function(req, res, next) {
 // No producción!!!11!!!11one!!1!
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

app.use(enableCORS)


const io = require('./io');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const externalAPI = require('./controllers/ExternalAPI');

const port = process.env.PORT || 3000;


app.listen(port);

console.log ("APItechU corriendo en puerto: " + port);

app.get("/apitechu/v1/users", userController.getUsersV1);
app.post("/apitechu/v1/users", userController.createUserV1);
app.delete("/apitechu/v1/users/:id", userController.deleteUserv1);

// Llamadas a la versión 2 de users
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);
app.post("/apitechu/v2/users", userController.createUserV2);

// Llamadas a la versión 3 de users
app.get("/apitechu/v3/users/:id", userController.getUsersByIdV3);
app.post("/apitechu/v3/users", userController.createUserV3);
app.post("/apitechu/v4/users", userController.createUserV4);


app.post("/apitechu/v1/login", authController.loginV1);
app.post("/apitechu/v1/logout", authController.logoutV1);

// Llamadas a la versión 2 de login/logout
app.post("/apitechu/v2/login", authController.loginV2);
app.post("/apitechu/v2/logout/:id", authController.logoutV2);

// app.get("/apitechu/v1/accounts/:id", accountController.getAccountsV1);
app.get("/apitechu/v2/users/:id/accounts", accountController.getAccountsV1);
app.post("/apitechu/v2/users/:id/newaccount", accountController.newAccountV1);

app.get("/apitechu/v1/user/:id/accounts/:idAccount/movements", accountController.getMovementsV1);
app.post("/apitechu/v1/user/:id/accounts/:idAccount/movements", accountController.newMovementV1);

app.get("/apitechu/getTiempoRivas", externalAPI.getTiempoRivas);

app.get("/apitechu/v1/hello",
  function(req,res){
    res.send({"msg" : "Hola"});
  }

);
