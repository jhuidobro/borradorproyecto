const bcrypt = require('bcrypt');

function hash (data) {
  console.log("Hasing Data");
  return bcrypt.hashSync(data, 10);
}

function checkpwd (sentPwd, userHshPwd) {
  console.log("Checking Pwd");
  console.log("Param 2: " + sentPwd);
  console.log("Param 2: " + userHshPwd);
  return bcrypt.compareSync(sentPwd, userHshPwd);
}

module.exports.hash = hash;
module.exports.checkpwd = checkpwd;
