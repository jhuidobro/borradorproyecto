const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should();

var server = require('../server');

describe('APITechu BANK test',
  function() {
    it('Test that APITechu BANK works',function(done) {
        chai.request('http://127.0.0.1:8081/index')
        .get('/')
        .end(
          function(err, res) {
            console.log("Request finished");
            // console.log(err);
            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }
)

describe('Test de API de Usuarios',
  function() {
    it('Prueba que la API de cuentas de APITechu BANK responde',function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v2/users/9/accounts')
        .end(
          function(err, res){
            console.log("Request finished");
            console.log(err);
            console.log("RESPUESTA res: " + res.body[0].balance);
            // console.log("RESPUESTA: " + res.body[0].balance);
            //console.log(res);
            res.should.have.status(200);
            // res.body[0].balance.should.be.eql(0);
            done();
          }
        )
      }
    )
    it('Prueba que la API de movimientos de APITechu BANK responde',function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/user/9/accounts/0182 2025 020 9 1/movements')
        .end(
          function(err, res){
            console.log("Request finished");
            console.log(err);
            // console.log("RESPUESTA movements: " + res.body.mensaje);
            res.should.have.status(200);
            res.body.mensaje.should.be.eql("La cuenta 0182 2025 020 9 1 no tiene movimientos");

            done();

          }
        )
      }
    )
  }
)
