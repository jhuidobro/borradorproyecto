# Dockerfile apitechu

#Imagen raiz
FROM node:6.0.0

# Carpeta Trabajo
WORKDIR /apitechu

# Añado archivos de mi aplicación a imagen
ADD . /apitechu

# Instalo los paquetes necesarios
RUN npm install

# Abrimos el puerto de la APItechU
EXPOSE 3000

# Comando de inicialización
CMD ["npm", "start"]
